import React, { useState, useContext } from "react";
import {
    Container,
    Row,
    Col,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
} from "reactstrap";
import { registerUser } from "../lib/auth";
import AppContext from "../context/AppContext";
import AuthLayout from "../components/Layout/AuthLayout";
import RegisterForm from "../components/RegisterForm";

const Register = () => {
    const [data, setData] = useState({ email: "", username: "", password: "" });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({});
    const appContext = useContext(AppContext);
    return (
        <AuthLayout page={"Créer un compte"}>
            <RegisterForm/> 
        </AuthLayout>
    );
};
export default Register;