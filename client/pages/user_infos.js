import React, { useState, useContext } from "react";
import { Row } from "reactstrap";
import AppContext from "../context/AppContext";
import UpdateForm from "../components/UpdateForm";
import UserInfosLayout from "../components/Layout/UserInfosLayout";

const UserInfos = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({});
    const userContext = useContext(AppContext);
    return (
        <UserInfosLayout
            page={"mon comtpe"}
            rubrique
        >{console.log(userContext)}
            <Row>
                {userContext.user &&
                    <UpdateForm
                        lastname={userContext.user.lastname}
                        username={userContext.user.username}
                        email={userContext.user.email}
                        password={userContext.user.password}
                        phone={userContext.user.phone}
                        address={userContext.user.address}
                        town={userContext.user.town}
                        zip_code={userContext.user.zip_code}
                    /> 
                }
            </Row>
        </UserInfosLayout>
        );
};
export default UserInfos;