import React from "react";
import Head from "next/head";
import App from "next/app";
import "../styles/style.scss";
import "../styles/layout.module.scss";
import "../styles/common.module.scss";
import "../styles/index.module.scss";
import "../styles/productlist.module.scss";
import "../styles/product.module.scss";
import "../styles/auth.module.scss";
import { ApolloProvider } from "@apollo/react-hooks";
import Cookie from "js-cookie";
import AppContext from "../context/AppContext";
import withData from "../lib/apollo";

class MyApp  extends App {
    state = {
        user: null,
    };
    
    componentDidMount() {
        // grab token value from cookie
        const token = Cookie.get("token");
    
        if (token) {
        // authenticate the token on the server and place set user object
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/me`, {
            headers: {
            Authorization: `Bearer ${token}`,
            },
        }).then(async (res) => {
            // if res comes back not valid, token is not valid
            // delete the token and log the user out on client
            if (!res.ok) {
            Cookie.remove("token");
            this.setState({ user: null });
            return null;
            }
            const user = await res.json();
            this.setUser(user);
        });
        }
    }
    setUser = (user) => {
        this.setState({ user });
    };

    render() {
        const { Component, pageProps, apollo } = this.props;
        console.log(this.props)
        return (
            <ApolloProvider client={apollo}>
                <AppContext.Provider
                    value={{
                        user: this.state.user,
                        isAuthenticated: !!this.state.user,
                        setUser: this.setUser,
                    }}
                > 
                    <Head>
                        <title>Strapi blog</title>
                        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                        <link
                            rel="stylesheet"
                            href="https://fonts.googleapis.com/css?family=Staatliches"
                        />
                        <link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2:wght@700&display=swap" rel="stylesheet"/> 
                        <link href="https://fonts.googleapis.com/css2?family=Rokkitt:wght@800&display=swap" rel="stylesheet"/> 
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
                        <link
                            rel="stylesheet"
                            href="https://cdn.jsdelivr.net/npm/uikit@3.2.3/dist/css/uikit.min.css"
                        />
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.min.js" />
                        <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.3/dist/js/uikit-icons.min.js" />
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.js" />
                    </Head>
                    <Component {...pageProps} />
                </AppContext.Provider>
            </ApolloProvider>
        );
    };
};

// Wraps all components in the tree with the data provider
export default withData(MyApp);