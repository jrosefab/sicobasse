import React, { useState, useContext } from "react";
import AppContext from "../context/AppContext";
import AuthLayout from "../components/Layout/AuthLayout";
import LoginForm from "../components/LoginForm";

const Login = () => {
    const [data, setData] = useState({ email: "", username: "", password: "" });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({});
    const appContext = useContext(AppContext);
    return (
        <AuthLayout page={"Se connecter"}>
            <LoginForm/> 
        </AuthLayout>
    );
};

export default Login;