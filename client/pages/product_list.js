import { useContext, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { useRouter } from "next/router";
import { gql } from "apollo-boost";
import ProductListLayout from "../components/Layout/ProductListLayout";
import AppContext from "../context/AppContext";
import { Col, Row,} from "reactstrap";
import ProductCard from "../components/ProductCard";

const GET_PRODUCT_LIST = gql`
query($id: ID!) {
    navigation(id: $id) {
        id
        name
        products {
            id
            title
            description
            price
            picture {
                url
            }
        }
    }
}
`;

const GET_FILTER = gql`
{
    filters{
        name
        products {
            id
        } 
    }
}
`;


function ProductList() {
    const appContext = useContext(AppContext);
    const router = useRouter();
    const { loading, error, data, ...autre } = useQuery(GET_PRODUCT_LIST, {
        variables: { id: router.query.id },
    });
    return (
        <ProductListLayout
            navigation={data && data.navigation && data.navigation.name}
            product_list={
                <Row>
                    {data && data.navigation &&
                        data.navigation.products.map((p) => (
                            <Col xs="6" sm="4" className="product_card_list" key={p.id}>
                                <ProductCard
                                    getProduct={() => router.push(`/product?id=${p.id}`)}
                                    picture={p.picture && `${process.env.NEXT_PUBLIC_API_URL}${p.picture[0].url}`}
                                    title={p.title}
                                    price={p.price}
                                />
                            </Col>
                        ))
                    }
                </Row>
            }
        />
    )
}

    export const getServerSideProps = async (ctx) => {
        return { props: {} };
    };

export default ProductList;