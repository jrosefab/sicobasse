import React, { useState } from "react";
import { Col, Input, InputGroup, InputGroupAddon, Row } from "reactstrap";
import HomeLayout from "../components/Layout/HomeLayout";
import ProductList from "../components/ProductList";
import Presentation from "../components/Presentation";
import SearchBar from "../components/SearchBar";
import Selection from "../components/Selection";
import Partenariats from "../components/Partenariats";
import DetailsProject from "../components/DetailsProject";

function Home() {
  const [query, updateQuery] = useState("");
  return (
    <HomeLayout 
      presentation={
        <Presentation searchbar={ 
          <SearchBar 
            onChange={e => updateQuery(e.target.value.toLocaleLowerCase())}
            value={query} 
          />
        }/>
      }
      details_project={<DetailsProject/>}
      selection={<Selection/>}
      partenariats={<Partenariats/>}
    >
    </HomeLayout>
  );
}
export default Home;