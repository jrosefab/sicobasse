import { useContext, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { useRouter } from "next/router";
import { gql } from "apollo-boost";
import AppContext from "../context/AppContext";
import ProductLayout from "../components/Layout/ProductLayout";
import ReactAudioPlayer from 'react-audio-player';
import { AiFillBank } from 'react-icons/ai';
import { GiPayMoney } from 'react-icons/gi';
import { FaCcVisa } from 'react-icons/fa';
import ImageGallery from 'react-image-gallery';

import {
    Button,
    Card,
    CardBody,
    CardImg,
    CardText,
    CardTitle,
    Col,
    Row,
    } from "reactstrap";

const GET_PRODUCT = gql`
    query($id: ID!) {
        product(id: $id) {
            id
            title
            description
            price
            picture {
                url
            }
            sound_like{
                url
            }
        }
        color {
            hexa
        }
    }
    `;

function Product() {
    const appContext = useContext(AppContext);
    const [slider, setSlider] = useState([])
    const router = useRouter();
    const { loading, error, data } = useQuery(GET_PRODUCT, {
        variables: { id: router.query.id },
    });
    if(data && data.product){
        const images = []
        data.product.picture.map(pic => {
            images.push({
                original : `${process.env.NEXT_PUBLIC_API_URL}${pic.url}`,
                thumbnail : `${process.env.NEXT_PUBLIC_API_URL}${pic.url}`,
                sizes :  "120" 
            })
        })
        return (
                <ProductLayout
                    item={data.product.title}
                    product={
                        <Row className="product_item">
                            <Col xs="12" lg="6" className="product_picture product_page">
                                <ImageGallery 
                                    thumbnailPosition={window.innerWidth <= 991 ? "bottom" : "left"}
                                    items={images} 
                                />
                               {/* <div style={{ backgroundImage : `url(${process.env.NEXT_PUBLIC_API_URL}${data.product[0].picture.url})` }}/> */}

                            </Col>
                            <Col xs="12" lg="6" className="product_details">
                                <div>
                                    <div className="product_details_top">
                                        <span style={{ color : `${data.color.hexa}`}}>
                                            {data.product.price}
                                        </span>
                                    </div>
                                    <div className="d-none d-md-block product_details_description">
                                        {data.product.description}
                                    </div>
                                </div>
                                {data.product.sound_like &&
                                    <ReactAudioPlayer
                                        src={`${process.env.NEXT_PUBLIC_API_URL}${data.product.sound_like.url}`}
                                        controls
                                    />
                                }
                                <div className="product_payment_container">
                                    <div className="product_payment">
                                        <p>Méthodes de paiement</p>
                                        <div className="product_payment_icon">
                                            <AiFillBank size={60}/>
                                            <GiPayMoney size={60}/>
                                            <FaCcVisa size={60}/>
                                        </div>
                                    </div>
                                    <div onClick={() => console.log(data.product.title)} className="add_to_basket" style={{ backgroundColor : `${data.color.hexa}`}}>
                                        Ajouter au panier
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    }
                >
            </ProductLayout>
        );
    }else{
        return (
            <ProductLayout/>
        )
    }
}

export default Product;