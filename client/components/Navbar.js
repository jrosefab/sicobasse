import React, { useContext } from 'react';
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import Link from "next/link";
import { Row,Col } from "reactstrap";
import { HiUser } from 'react-icons/hi';
import { BiLogOutCircle } from 'react-icons/bi';
import { RiShoppingBasketFill } from 'react-icons/ri';
import AppContext from "../context/AppContext";
import { logout } from "../lib/auth"

const QUERY = gql`
{
    navigations {
        id
        name
        link_to
    } 
}
`;

function Navbar() {
    const { loading, error, data } = useQuery(QUERY);
    const { user, setUser } = useContext(AppContext);

    if (error) return "";
    if (loading) return "";
    if (data.navigations && data.navigations.length) {
        return (
            <Row>
                <Col md={10} className="row">
                    {data.navigations && data.navigations.length &&
                        data.navigations.map((nav) => (
                            <Col md="2" key={nav.name} className="navigation_item">
                                <Link as={nav.link_to ==="" ? "/" : `/product_list/${nav.id}`}
                                    href={nav.link_to ==="" ? "/" : `/product_list?id=${nav.id}`}>
                                        <a className="">
                                            {nav.name}
                                        </a>
                                </Link>
                            </Col>
                        ))
                    }
                </Col>
                <Col md={2} className="user_infos">
                    <Link href={user ? "/user_infos" : "/login"}>
                        <div>
                            <HiUser size={30}/>
                            { user ?
                                <a>Bonjour {user.username}</a>
                                :
                                <a>Se connecter</a>
                            }
                        </div>
                    </Link>
                    <div>
                    <RiShoppingBasketFill size={30}/>
                    </div>
                    { user && 
                        <div onClick={ () => { logout(); setUser(null)}}>
                            <BiLogOutCircle size={30}/>
                        </div>
                    }
                </Col>
                <style jsx global>
                    {`
                        .navigation_item {
                            text-transform : uppercase;
                            padding: 1rem 1.5rem;
                            margin-top : 5px;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            white-space : nowrap;
                        }
                    `}
                </style>
            </Row>
        )}
} 

export default Navbar;