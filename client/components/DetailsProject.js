import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Row, Col } from "reactstrap";

const QUERY = gql`
{
    presentation {
        details_project
    } 
    collaborators(where : {role : director}){
        firstname
        lastname
        description
        email
        city
        address
        number_phone
        picture {
            url
        }
        zip_code
    }
}
`;

function DetailsProject() {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "";
    if (loading) return "";
    if (data.presentation && data.collaborators) {
        return (
            <Row className="p-2 details_project_container">
                <Col lg="6" className="mt-4">
                        <div className="details_project_image" 
                            style={data.collaborators && { backgroundImage : `url(${process.env.NEXT_PUBLIC_API_URL}${data.collaborators[0].picture.url})`}}
                        >
                        <div className="details_project_text">
                            <h2>{data.collaborators[0].firstname} {data.collaborators[0].lastname}</h2>
                            <p>{data.collaborators[0].description}</p>
                        </div>
                    </div>
                </Col>
                <Col lg="6" className="mt-4">
                    <div className="details_project_maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1930.1617771804865!2d-61.02538754185469!3d14.637565897444745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c6aa15dfd60db53%3A0x4cb31f678650240!2sChemin%20Cl%C3%A9mencin%2C%20Le%20Lamentin%2C%20Martinique!5e0!3m2!1sfr!2sfr!4v1600273929768!5m2!1sfr!2sfr" 
                            width="100%" 
                            frameBorder="0" 
                            style={{ border : 0 }} 
                            allowFullScreen="" 
                            aria-hidden="false" 
                            tabIndex="0"
                        />
                        <div className="details_project_text">
                            <h2>Testez nos instruments</h2>
                            <p>{data.collaborators[0].description}</p>
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }else{
    return <h1>{console.log(data)}No Logo Found</h1>;
    } 
}

export default DetailsProject;