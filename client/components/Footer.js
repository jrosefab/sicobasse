import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Container, Row, Col} from 'reactstrap';
import Logo from "./Logo";
import { RiFacebookCircleFill } from 'react-icons/ri';
import { FaLinkedin } from 'react-icons/fa';
import { TiSocialInstagramCircular } from 'react-icons/ti';
import { FaTwitterSquare } from 'react-icons/fa';
import { FaWhatsappSquare } from 'react-icons/fa';
import { TiSocialYoutubeCircular } from 'react-icons/ti';

const QUERY = gql`
{
    footer{
        picture {
            url
        }
    }
    socials {
        name
        link_to
    } 
    entreprise{
        city
        address
        zip_code
        description
    }
}
`;

const SN_ICONS = [
    {
        name : "Facebook",
        icon : <RiFacebookCircleFill size={60}/>
    },
    {
        name : "Linkedin",
        icon : <FaLinkedin size={60}/>
    },
    {
        name : "Instagram",
        icon : <TiSocialInstagramCircular size={60}/>
    },
    {
        name : "Twitter",
        icon : <FaTwitterSquare size={60}/>
    },
    {
        name : "Whatsapp",
        icon : <FaWhatsappSquare size={60}/>
    },
    {
        name : "Youtube",
        icon : <TiSocialYoutubeCircular size={60}/>
    }
]

function Footer() {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "Error loading bg";
    if (loading) return "";
    return (
        <div className="footer_container pt-5" style={data.footer && { backgroundImage : `url(${process.env.NEXT_PUBLIC_API_URL}${data.footer.picture.url})`}}>
            <Col sm="8 offset-sm-2 row footer_picture">
                <Col md="12" className="footer_item">
                    <Logo/>
                </Col>
                <Col md="12" className="footer_item mt-3 mb-3">
                    {data.entreprise.description}<br/>
                    {data.entreprise.address} {data.entreprise.zip_code} <br/>
                    {data.entreprise.city}
                </Col>
                <Col md="12" className="footer_item mb-3">
                    <div className="footer_socials">
                    { data.socials.map(sn=> (
                        SN_ICONS.map(i =>{
                            if(sn.name === i.name){
                                return (
                                    <div key={i.name}>
                                        {i.icon}
                                    </div>
                                )
                            }
                        })
                    ))}
                    </div>
                </Col>
                <Col md="12" className="footer_item mb-3">
                    © Colin Ingram Ltd. Website by Dewynters | Terms and Conditions | Privacy Policy | Cookies Policy
                </Col>
            </Col>
            <style jsx>
            {`
                
            `}
            </style>
        </div>
        );
}

export default Footer;