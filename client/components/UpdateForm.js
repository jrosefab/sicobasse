import React, { useState, useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { registerUser } from "../lib/auth";
import { Container, Row, Col} from 'reactstrap';

const QUERY = gql`
{
    color{
        hexa
    }
}
`;

function UpdateForm(props) {
    const { data } = useQuery(QUERY);
    const [user, setUser] = useState({
        lastname: "", 
        username: "", 
        email: "", 
        confirm_email: "", 
        password: "", 
        confirm_password: "", 
        phone: "", 
        address: "", 
        town: "",
        zip_code: ""
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({});

    const submitUser = (e) => {
        e.preventDefault();
        
        setLoading(true);
        const { confirm_password, confirm_email, ...data} = user
        console.log(data)
        registerUser(data)
        .then((res) => {
            console.log("res", res)
            // set authed user in global context object
            appContext.setUser(res.data.user);
            setLoading(false);
        })
        .catch((error) => {
            console.log("error", error)
            setError(error);
            setLoading(false);
        });
    }
    return (
        <div className="form_container user_page">
            <Col sm="8 offset-sm-2 row" className="form_bg form_text">
                <Col xs="12">
                    <form onSubmit={submitUser} className="row form">
                        <Col xs="6" className="mt-3">
                            <input placeholder={props.lastname}
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, lastname: e.target.value })
                                }
                                value={user.lastname}
                            />
                        </Col>
                        <Col xs="6" className="mt-3">
                            <input placeholder={props.username}
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, username: e.target.value })
                                }
                                value={user.username}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder={props.email}
                                type="email"
                                onChange={(e) =>
                                    setUser({ ...user, email: e.target.value })
                                }
                                value={user.email}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder={props.password}
                                type="password"
                                onChange={(e) =>
                                    setUser({ ...user, password: e.target.value })
                                }
                                value={user.password}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder={props.phone} 
                                type="number"
                                onChange={(e) =>
                                    setUser({ ...user, phone: e.target.value })
                                }
                                value={user.phone}
                            />                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder={props.address}
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, address: e.target.value })
                                }
                                value={user.address}
                            />  
                        </Col>
                        <Col xs="6" className="mt-3">
                            <input placeholder={props.town}
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, town: e.target.value })
                                }
                                value={user.town}
                            />  
                        </Col>
                        <Col xs="6" className="mt-3">
                            <input placeholder={props.zip_code}
                                type="number"
                                onChange={(e) =>
                                    setUser({ ...user, zip_code: e.target.value })
                                }
                                value={user.zip_code}
                            />                         
                        </Col>
                        <button type='submit'
                            disabled={loading}>
                            Mêtre à jour
                        </button>
                    </form>
                </Col>
            </Col>
        </div>
    );
}

export default UpdateForm;