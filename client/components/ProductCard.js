import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { RiShoppingBasketFill } from 'react-icons/ri';

const QUERY = gql`
{
    color{
        hexa
    }
}
`;

function ProductCard(props) {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "";
    if (loading) return "";
    if (data && data.color) {
        return (
            <div className="product_card_container">
                <div className="product_card_picture" 
                    onClick={props.getProduct}
                    style={{ backgroundImage : `url(${props.picture})` }}
                />
                <div className="product_card_info" onClick={props.getProduct}>
                    <span>{props.title}</span>
                    <span style={{ color : `${data.color.hexa}`}}>
                        {props.price}
                    </span>
                </div>
                <div onClick={props.addToBascket} className="add_to_basket" style={{ backgroundColor : `${data.color.hexa}` }}>
                    <RiShoppingBasketFill size={30}/>
                </div>
            </div>
        );
    }
}

export default ProductCard;