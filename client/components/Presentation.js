import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const QUERY = gql`
{
    presentation {
        title
        description
        subtitle
    }
}
`;

function Presentation(props) {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "Error loading presentation";
    if (loading) return "";
    if (data.presentation) {
        return (
            <div className="presentation_container"> 
                <h1 className="presentation_item title">
                    {data.presentation.title}
                </h1>
                <h2 className="presentation_item title">
                    {data.presentation.subtitle}
                </h2>
                <h3 className="presentation_item">
                    {data.presentation.description}
                </h3>
                {props.searchbar}
            </div>
        );
    } else {
        return "";
    }
}

export default Presentation;