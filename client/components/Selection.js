import React, { useState } from "react";
import Carousel from 'react-elastic-carousel';
import { Row,Col } from "reactstrap";
import { gql } from "apollo-boost";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/react-hooks";
import { FiArrowRightCircle } from 'react-icons/fi';
import { FiArrowLeftCircle } from 'react-icons/fi';

const QUERY = gql`
{
    products(where : {selection : true}){
        id
        title
        description
        price
        picture {
            url
        }
        sound_like {
            url
        }
    }
    color{
        hexa
    }
}
`;

const customArrow = ({ type, onClick, isEdge }) => {
    return (
        <>
            {type === 'PREV' ?
                <FiArrowLeftCircle 
                    className="carroussel_arrow"
                    onClick={onClick} 
                    size={90}
                /> : 
                <FiArrowRightCircle 
                    className="carroussel_arrow"
                    onClick={onClick} 
                    size={90}
                />
            }
        </>
    )
}

function Selection() {
    const [query, updateQuery] = useState("");
    const { loading, error, data } = useQuery(QUERY);
    const router = useRouter();
    if (error) return "";
    if (loading) return "";
    if (data && data.products) {
        return (
            <Carousel itemsToShow={1} 
                    renderPagination={() => ( <></>)}
                    renderArrow={customArrow}
            >
                {data.products.map((p) =>(
                    <Row key={p.id} className="product_item">
                        <Col xs="12" lg="6" className="product_picture selection_page">
                            <div onClick={() => router.push(`/product?id=${p.id}`)} 
                                style={{ backgroundImage : `url(${process.env.NEXT_PUBLIC_API_URL}${p.picture[0].url})` }}
                            />
                        </Col>
                        <Col xs="12" lg="6" className="product_details">
                            <div>
                                <div className="product_details_top" onClick={() => router.push(`/product?id=${p.id}`)}>
                                    <span>{p.title}</span>
                                    <span style={{ color : `${data.color.hexa}`}}>
                                        {p.price}
                                    </span>
                                </div>
                                <div className="d-none d-md-block product_details_description">
                                    {p.description}
                                </div>
                            </div>
                            <div onClick={() => console.log(p.title)} className="add_to_basket" style={{ backgroundColor : `${data.color.hexa}`}}>
                                Ajouter au panier
                            </div>
                        </Col>
                    </Row>
                ))}
            </Carousel>
        );
    }else{
        return ( 
            <div>
                Error
            </div>
        )
    }
}
export default Selection;