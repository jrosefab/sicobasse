import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

function SearchBar(props) {
    return (
        <div className="input_header"> 
            <input onChange={props.onChange} 
                value={props.query}
                className="input"
                placeholder="Que rechercher vous ?"
            />
        </div>
    );
}

export default SearchBar;