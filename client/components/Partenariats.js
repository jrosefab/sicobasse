import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Row, Col } from "reactstrap";
import Link from "next/link";

const QUERY = gql`
{
    partenariats {
        name
        picture {
            url
        }
    } 
}
`;

function Partenariats() {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "";
    if (loading) return "";
    return (
        data.partenariats && data.partenariats.length &&
            <Row>
                {data.partenariats.map((par) => (
                    <Col key={par.name} xs="6" md="4" lg="3" className="partenariats_container">
                        <div className="partenariats_image" 
                            style={par.picture && { backgroundImage : `url(${process.env.NEXT_PUBLIC_API_URL}${par.picture.url})`}}
                        >
                        </div>
                    </Col>
                    )
                )}
            </Row>
    )
} 

export default Partenariats;