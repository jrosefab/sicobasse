import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const QUERY = gql`
{
    presentation {
        picture {
            url
        }
    }
}
`;

function BackgroundHeader() {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "Error loading bg";
    if (loading) return "";
    if (data.presentation) {
        return (
            <div className="background_header"
                style={{ backgroundImage : `url(${process.env.NEXT_PUBLIC_API_URL}${data.presentation.picture.url})`}}
            >
                <style jsx>
                {`
                    .background_header {
                        position : absolute;
                        width : 100%;
                        z-index: 0;
                        height: 100%;
                        background-size: cover;
                        background-repeat: no-repeat;
                    }
                `}
                </style>
            </div>
        );
    } else {
        return "";
    }
}

export default BackgroundHeader;