import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const QUERY = gql`
{
    logo {
        picture{
            url
        }
    } 
}
`;

function Logo() {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "Error loading logo";
    if (loading) return "";
    if (data.logo) {
        return (
            <>
                <img src={`${process.env.NEXT_PUBLIC_API_URL}${data.logo.picture.url}`} 
                    className="img-fluid logo" 
                    alt="Responsive image"
                />
                <style jsx>
                {`
                    .logo {
                        height: 80px;
                        width: 120px;
                    }
                `}
                </style>
            </>
        );
    } else {
        return <h1>No Logo Found</h1>;
    }
}

export default Logo;