import React from 'react';
import { Container, Row, Col} from 'reactstrap';
import { useRouter } from "next/router";
import Logo from '../Logo';
import Link from "next/link";
import Navbar from '../Navbar';
import BackgroundHeader from '../BackgroundHeader';
import Footer from '../Footer';


const UserInfosLayout = (props) => {
    const router = useRouter();

    return (
        <Container fluid>{console.log(router)}
            <Row className="header_container">
                <BackgroundHeader/>
                <Col xs={12} md={2} className="d-flex justify-content-center">
                    <Logo/>
                </Col>
                <Col md={10} className="d-none d-md-block justify-content-center">
                    <Navbar/>
                </Col>
                <Col md={8} className="offset-md-2 mt-5 navigations">
                    <span onClick={() => router.back()}>Accueil</span>
                    &ensp;›&ensp;
                    <span>{props.navigation}</span>
                </Col>
                <Col md={8} className="offset-md-3 mt-5 category_title">
                    <h1>{props.page}</h1>
                </Col>
                <Col md="12" className="mb-5">
                    <Row>
                        <Col md="3" className="sidebar_items">
                            <div className={router.pathname === "/user_infos" ? "active" : "" }>
                                <Link href="/user_infos">
                                    <a>Mon compte</a>
                                </Link>
                            </div>
                            <div className={router.pathname === "command_list" ? "active" : "" }>
                                <Link href="/">
                                    <a>Mes commandes</a>
                                </Link>
                            </div>
                            <div className={router.pathname === "faq" ? "active" : "" }>
                                <Link href="/">
                                    <a>FAQ</a>
                                </Link>
                            </div>
                            <div className={router.pathname === "conditions" ? "active" : "" }>
                                <Link href="/">
                                    <a>Conditions générales</a>
                                </Link>
                            </div>
                        </Col>
                        <Col md="8">{props.children}</Col>
                    </Row>
                </Col>
            </Row>
            <Row style={{ marginTop : "auto" }}>
                <Footer/>
            </Row>
        </Container>
    )
}

export default UserInfosLayout;