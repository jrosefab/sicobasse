import React from 'react';
import { Container, Row, Col} from 'reactstrap';
import { useRouter } from "next/router";
import Logo from '../Logo';
import Navbar from '../Navbar';
import BackgroundHeader from '../BackgroundHeader';
import Footer from '../Footer';


const ProductListLayout = (props) => {
    const router = useRouter();
    return (
        <Container fluid>
            <Row className="header_container">
                <BackgroundHeader/>
                <Col xs={12} md={2} className="d-flex justify-content-center">
                    <Logo/>
                </Col>
                <Col md={10} className="d-none d-md-block justify-content-center">
                    <Navbar/>
                </Col>
                <Col md={8} className="offset-md-3 mt-5 navigations">
                    <span onClick={() => router.back()}>Accueil</span>
                    &ensp;›&ensp;
                    <span>{props.navigation}</span>
                </Col>
                <Col md={8} className="offset-md-3 mt-5 category_title">
                    <h1>{props.navigation}</h1>
                </Col>
                <Col md="12" className="mb-5">
                    <Row>
                        <Col md="3">{props.product_filter}</Col>
                        <Col md="8">{props.product_list}</Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mt-5">
            <Col sm="8 offset-sm-2 add_basket">
                <h1>Vous pourrez appréciez</h1>
            </Col>
            </Row>
            <Row style={{ marginTop : "auto" }}>
                <Footer/>
            </Row>
        </Container>
    )
}

export default ProductListLayout;