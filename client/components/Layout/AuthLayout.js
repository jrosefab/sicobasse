import React from 'react';
import { Container, Row, Col} from 'reactstrap';
import Logo from '../Logo';
import Navbar from '../Navbar';
import BackgroundHeader from '../BackgroundHeader';
import Footer from '../Footer';


const AuthLayout = (props) => {
    return (
        <Container fluid>
            <Row className="header_container">
                <BackgroundHeader/>
                <Col xs={12} md={2} className="d-flex justify-content-center">
                    <Logo/>
                </Col>
                <Col md={10} className="d-none d-md-block justify-content-center">
                    <Navbar/>
                </Col>
                <Col md={8} className="offset-md-2 mt-5 navigations">
                    <span onClick={() => router.back()}>Accueil</span>
                    &ensp;›&ensp;
                    <span>{props.navigation}</span>
                </Col>
                <Col md={8} className="offset-md-2 mt-5 category_title">
                    <h1>{props.page}</h1>
                </Col>
                <Col md={8} className="offset-md-2  mb-5 category_title">
                    {props.children}
                </Col>
            </Row>
            <Row style={{ marginTop : "auto" }}>
                <Footer/>
            </Row>
        </Container>
    )
}

export default AuthLayout;