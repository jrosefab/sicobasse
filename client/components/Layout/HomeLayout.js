import React from 'react';
import { Container, Row, Col} from 'reactstrap';
import Logo from '../Logo';
import Navbar from '../Navbar';
import BackgroundHeader from '../BackgroundHeader';
import NavbarWithImage from '../NavbarWithImage';
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import Link from "next/link";
import CoursesForm from '../CoursesForm';
import Footer from '../Footer';

const QUERY = gql`
{
    rubriques {
        title
    }
}
`;

const HomeLayout = (props) => {
    const { loading, error, data } = useQuery(QUERY);
    return (
        <Container fluid>
            <Row className="header_container">
            <BackgroundHeader/>
            <Col xs={12} md={2} className="d-flex justify-content-center">
                <Logo/>
            </Col>
            <Col md={10} className="d-none d-md-block justify-content-center">
                <Navbar/>
            </Col>
            <Col sm="8 offset-sm-2">
                {props.presentation}
                <div className="d-none d-md-block">
                {props.informations}  
                </div>
            </Col>
            </Row>
            { data && data.rubriques &&
                <>
                    <Row className="mt-5">
                        <Col sm="8 offset-sm-2">
                        <h4 className="rubrique_item">
                            {data.rubriques[0].title}
                        </h4>   
                        {props.selection} 
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col sm="8 offset-sm-2">
                        <h4 className="rubrique_item">
                            {data.rubriques[1].title}
                        </h4>  
                        {props.details_project}  
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col sm="8 offset-sm-2">
                        <h4 className="rubrique_item">
                            {data.rubriques[2].title}
                        </h4>
                        <NavbarWithImage/>      
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col sm="8 offset-sm-2">
                        <h4 className="rubrique_item">
                            {data.rubriques[3].title}
                        </h4>    
                        {props.partenariats}
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col sm="8 offset-sm-2">
                        <h4 className="rubrique_item">
                            {data.rubriques[4].title}
                        </h4>
                        </Col>
                        <CoursesForm/>
                    </Row>
                </>
            }
            <Row style={{ marginTop : "auto" }}>
                <Footer/>
            </Row>
        </Container>
    )
}

export default HomeLayout;