import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Row, Col } from "reactstrap";
import Link from "next/link";

const QUERY = gql`
{
    navigations {
        id
        name
        link_to
        picture {
            url
        }
    } 
}
`;

function NavbarWithImage() {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "";
    if (loading) return "";
    if(data.navigations[0].name === "Accueil"){
        data.navigations.shift()
    }
    if (data.navigations && data.navigations.length) {
        return (
            <Row className="p-4">
                {data.navigations.map((nav) => (
                    <Col key={nav.name} xs="6" lg="" className="p-0 navbar_pictures_container">
                        <Link as={`/product_list/${nav.id}`}
                            href={`/product_list?id=${nav.id}`}>
                            <div className="navbar_picture_image" 
                                style={nav.picture && { backgroundImage : `url(${process.env.NEXT_PUBLIC_API_URL}${nav.picture.url})`}}
                            >
                                <div className="navbar_picture_text">
                                    <p>{nav.name}</p>
                                </div>
                            </div>
                            
                        </Link>
                    </Col>
                )
            )}
            </Row>
        )
    }else{
        return <h1>No Logo Found</h1>;
    } 
}

export default NavbarWithImage;