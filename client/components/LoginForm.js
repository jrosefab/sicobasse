import React, { useState, useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { useRouter } from "next/router";
import { gql } from "apollo-boost";
import AppContext from "../context/AppContext";
import { login } from "../lib/auth";
import { Container, Row, Col} from 'reactstrap';

const QUERY = gql`
{
    color{
        hexa
    }
}
`;

function LoginForm() {
    const { data } = useQuery(QUERY);
    const router = useRouter();
    const [user, setUser] = useState({
        email: "", 
        password: "", 
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({});
    const appContext = useContext(AppContext);

    const loginUser = (e) => {
        e.preventDefault();
        
        setLoading(true);
        const { email, password } = user
        login(email, password)
        .then((res) => {
            console.log("res", res)
            appContext.setUser(res.data.user);
            setLoading(false);
        })
        .catch((error) => {
            console.log("error", error)
            setError(error);
            setLoading(false);
        });
    }
    return (
        <div className="form_container auth_page" style={data && data.color && { backgroundColor : `${data.color.hexa}` }}>
            <Col sm="8 offset-sm-2 row" className="form_bg form_text">
                <Col xs="12">
                    <form onSubmit={loginUser} className="row form">
                        <Col xs="12" className="mt-3">
                            <input placeholder="ADDRESSE EMAIL"
                                type="email"
                                onChange={(e) =>
                                    setUser({ ...user, email: e.target.value })
                                }
                                value={user.email}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder="MOT DE PASSE"
                                type="password"
                                onChange={(e) =>
                                    setUser({ ...user, password: e.target.value })
                                }
                                value={user.password}
                            />
                        </Col>
                        <Col md="6" className="justify-content-center d-flex">
                            <button onClick={() => router.push('/register')}>
                                Je n'ai pas de comtpe
                            </button>
                        </Col>
                        <Col md="6" className="justify-content-center d-flex">
                            <button type='submit'
                                    disabled={loading}>
                                se connecter
                            </button>
                        </Col>
                    </form>
                </Col>
            </Col>
        </div>
    );
}

export default LoginForm;