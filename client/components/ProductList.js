import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import Link from "next/link";
import ReactAudioPlayer from 'react-audio-player';

import {
    Card,
    CardBody,
    CardImg,
    CardText,
    CardTitle,
    Row,
    Col,
} from "reactstrap";

const QUERY = gql`
{
    products {
        id
        title
        description
        picture {
            url
        }
        sound_like {
            url
        }
    }
    logo {
        picture{
            url
        }
    } 
}
`;

function ProductList(props) {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "Error loading products";
  //if restaurants are returned from the GraphQL query, run the filter query
  //and set equal to variable restaurantSearch
    if (loading) return <h1>Fetching</h1>;
    if (data.products && data.products.length) {
    //searchQuery
    const searchQuery = data.products.filter((query) => (
            query.title.toLowerCase().includes(props.search)
    ));

    if (searchQuery.length != 0) {
        return (
            <Row>
            {searchQuery.map((p) => (
                <Col xs="6" sm="4" key={p.id}>
                <Card style={{ margin: "0 0.5rem 20px 0.5rem" }}>
                    <CardImg
                        top={true}
                        style={{ height: 250 }}
                        src={p.picture && `${process.env.NEXT_PUBLIC_API_URL}${p.picture.url}`}
                    />
                    <CardBody>
                    <CardTitle>{p.title}</CardTitle>
                    <CardText>{p.description}</CardText>
                    </CardBody>
                    <div className="card-footer">
                    <Link
                        as={`/product/${p.id}`}
                        href={`/product?id=${p.id}`}
                    >
                        <a className="btn btn-primary">View</a>
                    </Link>
                    {p.sound_like &&
                        <ReactAudioPlayer
                            src={`${process.env.NEXT_PUBLIC_API_URL}${p.sound_like.url}`}
                            controls
                        />
                    }
                    </div>
                </Card>
                </Col>
            ))}

            <style jsx global>
                {`
                a {
                    color: white;
                }
                a:link {
                    text-decoration: none;
                    color: white;
                }
                a:hover {
                    color: white;
                }
                .card-columns {
                    column-count: 3;
                }
                `}
            </style>
            </Row>
        );
        } else {
            return <h1>No instruments Found</h1>;
        }
    }
    return <h5>Add instruments</h5>;
}
export default ProductList;