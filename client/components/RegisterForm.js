import React, { useState, useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { useRouter } from "next/router";
import { gql } from "apollo-boost";
import { registerUser } from "../lib/auth";
import AppContext from "../context/AppContext";
import { Container, Row, Col} from 'reactstrap';
import Link from "next/link";

const QUERY = gql`
{
    color{
        hexa
    }
}
`;

function RegisterForm() {
    const { data } = useQuery(QUERY);
    const router = useRouter();
    const [user, setUser] = useState({
        lastname: "", 
        username: "", 
        email: "", 
        confirm_email: "", 
        password: "", 
        confirm_password: "", 
        phone: "", 
        address: "", 
        town: "",
        zip_code: ""
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({});

    const appContext = useContext(AppContext);
    const submitUser = (e) => {
        e.preventDefault();
        
        setLoading(true);
        const { confirm_password, confirm_email, ...data} = user
        console.log(data)
        registerUser(data)
        .then((res) => {
            console.log("res", res)
            appContext.setUser(res.data.user);
            setLoading(false);
        })
        .catch((error) => {
            console.log("error", error)
            setError(error);
            setLoading(false);
        });
    }
    return (
        <div className="form_container auth_page" style={data && data.color && { backgroundColor : `${data.color.hexa}` }}>
            <Col sm="8 offset-sm-2 row" className="form_bg form_text">
                <Col xs="12">
                    <form onSubmit={submitUser} className="row form">
                        <Col xs="6" className="mt-3">
                            <input placeholder="NOM" 
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, lastname: e.target.value })
                                }
                                value={user.lastname}
                            />
                        </Col>
                        <Col xs="6" className="mt-3">
                            <input placeholder="PRÉNOM"
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, username: e.target.value })
                                }
                                value={user.username}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder="ADDRESSE EMAIL"
                                type="email"
                                onChange={(e) =>
                                    setUser({ ...user, email: e.target.value })
                                }
                                value={user.email}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder="CONFIRMER VOTRE ADDRESSE EMAIL"
                                type="email"
                                onChange={(e) =>
                                    setUser({ ...user, confirm_email: e.target.value })
                                }
                                value={user.confirm_email}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder="MOT DE PASSE"
                                type="password"
                                onChange={(e) =>
                                    setUser({ ...user, password: e.target.value })
                                }
                                value={user.password}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder="CONFIRMER VOTRE MOT DE PASSE"
                                type="password"
                                onChange={(e) =>
                                    setUser({ ...user, confirm_password: e.target.value })
                                }
                                value={user.confirm_password}
                            />
                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder="NUMÉRO DE TÉLÉPHONE" 
                                type="number"
                                onChange={(e) =>
                                    setUser({ ...user, phone: e.target.value })
                                }
                                value={user.phone}
                            />                        </Col>
                        <Col xs="12" className="mt-3">
                            <input placeholder="ADDRESSE POSTALE"
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, address: e.target.value })
                                }
                                value={user.address}
                            />  
                        </Col>
                        <Col xs="6" className="mt-3">
                            <input placeholder="VILLE"
                                type="text"
                                onChange={(e) =>
                                    setUser({ ...user, town: e.target.value })
                                }
                                value={user.town}
                            />  
                        </Col>
                        <Col xs="6" className="mt-3">
                            <input placeholder="CODE POSTALE"
                                type="number"
                                onChange={(e) =>
                                    setUser({ ...user, zip_code: e.target.value })
                                }
                                value={user.zip_code}
                            />                         
                        </Col>
                        <Col lg="6" className="justify-content-center d-flex">
                            <button onClick={() => router.push('/login')}>
                                J'ai déjà un compte
                            </button>
                        </Col>
                        <Col lg="6" className="justify-content-center d-flex">
                            <button type='submit'
                            disabled={loading}>
                                créer mon compte
                            </button>
                        </Col>
                    </form>
                </Col>
            </Col>
        </div>
    );
}

export default RegisterForm;