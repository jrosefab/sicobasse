import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Container, Row, Col} from 'reactstrap';

const QUERY = gql`
{
    color{
        hexa
    }
}
`;

function CoursesForm() {
    const { loading, error, data } = useQuery(QUERY);
    if (error) return "Error loading bg";
    if (loading) return "";
    if (data.color) {
        return (
            <div className="form_container">
                <div className="form_bg home_page"></div>
                <Col sm="8 offset-sm-2 row" className="form_text">
                    <Col xs="12" md="6">
                        Real coaching equals real results. We've partnered with the best trainers in the industry to bring you a coaching experience like none other.
                    </Col>
                    <Col xs="12" md="6">
                        <form className="row form">
                            <Col xs="6" className="mt-3">
                                <input placeholder="NOM" type="text"></input>
                            </Col>
                            <Col xs="6" className="mt-3">
                                <input placeholder="PRÉNOM" type="text"></input>
                            </Col>
                            <Col xs="12" className="mt-3">
                                <input placeholder="ADDRESSE EMAIL" type="text"></input>
                            </Col>
                            <Col xs="12" className="mt-3">
                                <input placeholder="NUMÉRO DE TÉLÉPHONE" type="text"></input>
                            </Col>
                            <button>Je m'inscrit</button>
                        </form>
                    </Col>
                </Col>
                <style jsx>
                {`
                    .form_bg {
                        background-color : ${data.color.hexa};
                    }
                `}
                </style>
            </div>
        );
    } else {
        return "";
    }
}

export default CoursesForm;